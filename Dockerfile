FROM python:latest

ADD ./app.py .

RUN pip install virtualenv
RUN pip install Flask

CMD ["python", "./app.py"]
