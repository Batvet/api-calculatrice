from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/hello')
def hello():
    return 'Hello welcome to the ESBay User API\n'

@app.route('/api/add', methods=['GET'])
def add():
    number1 = request.args.get('a', type=int) 
    number2 = request.args.get('b', type=int) 
    if number1 is None or number2 is None: 
        return jsonify({'error': 'Invalid input please provide two numbers'}), 400
    result = number1 + number2
    return jsonify({'result': result})

# Fonction de soustraction
@app.route('/api/subtract', methods=['GET'])
def subtract():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input please provide two numbers'}), 400
    result = number1 - number2
    return jsonify({'result': result})

# Fonction de multiplication
@app.route('/api/multiply', methods=['GET'])
def multiply():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input please provide two numbers'}), 400
    result = number1 * number2
    return jsonify({'result': result})

# Fonction de division
@app.route('/api/divide', methods=['GET'])
def divide():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None or number2 == 0:
        return jsonify({'error': 'Invalid input please provide two numbers and b cannot be zero'}), 400
    result = number1 / number2
    return jsonify({'result': result})

# test
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)